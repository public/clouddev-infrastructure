# Cloud Development Infrastructure

This repository contains playbooks to setup a local [MicroK8s](https://microk8s.io/) cluster.
[Terraform](https://www.terraform.io/) configuration and [Ansible](https://docs.ansible.com/ansible/latest/index.html) playbooks to create a cluster using a cloud provider are contained as well.

The following instructions show how to setup the local MicroK8s cluster.

## Requirements

It is assumed that you have all tools from the [Cloud Development Environment](https://gitea.eseidinger.de/public/clouddev-env) installed.

## Cluster Infrastructure

To be able to use observability features and advanced deployment scenarios, you need to install some cluster infrastructure.

### Basic Setup

The local MicroK8s cluster needs to be configured to enable auditing.
Some addons, like a hostpath storage provisioner will also be installed.

```bash
cd k8s/ansible
ansible-playbook -i inventory/local playbooks/config-local-microk8s.yml
```

After running the command above you must set the *KUBECONFIG* environment variable to the config file for the local MicroK8s cluster.

```bash
cd k8s
source activate_local_env.sh
```

You need to to do this in every shell you want to run the [kubectl](https://kubectl.docs.kubernetes.io/) command or [k9s](https://k9scli.io/) on the local cluster.
The ansible tasks already have the correct environment variable set.

### Networking and Observability

A [MetalLB](https://metallb.universe.tf/) load balancer, an [Istio](https://istio.io/) service mesh and [cert-manager](https://cert-manager.io/) need to be installed.

```bash
cd k8s/ansible
ansible-playbook -i inventory/local playbooks/deploy-networking-stack.yml
```

An observability stack will be installed as well. The stack includes [Grafana](https://grafana.com/) for dashboards, [Prometheus](https://prometheus.io/) for collecting metrics, [Loki](https://grafana.com/oss/loki/) for log aggregation,
[Jaeger](https://www.jaegertracing.io/) for tracing, [Kiali](https://kiali.io/), as console for Istio, and [Falco](https://sysdig.com/opensource/falco/) for Kubernetes auditing.

```bash
cd k8s/ansible
ansible-playbook -i inventory/local playbooks/deploy-observability-stack.yml
```

When the deployment has finished you can view the Grafana UI by navigating to [http://grafana.127-0-0-1.nip.io](http://grafana.127-0-0-1.nip.io). The username is set to *admin* and the password is set to *admin123*.
The hostname ending with *127-0-0-1.nip.io* is resolved to point to the localhost address. You have to make sure port 80 is forwarded to your host running the browser when working in a VM.

The other dashboards don't have an ingress, so you'll have to forward the service ports to your VM and / or host.
You can either use *kubectl* or *k9s* to achieve port-forwarding between the cluster and the machine running *kubectl* or *k9s*. When you work remotely using [Visual Studio Code](https://code.visualstudio.com/docs/remote/ssh), you can use its [port forwarding feature](https://code.visualstudio.com/docs/remote/ssh#_forwarding-a-port-creating-ssh-tunnel) to forward ports from the remote machine / VM to your host running the browser.

To access *Prometheus*, execute:

```bash
kubectl port-forward -n observability svc/prometheus-operated 9090:9090
```

For *Jaeger*, execute:

```bash
kubectl port-forward -n observability svc/simplest-query 16686:16686
```

*Falco* audit logs can be viewed with the sidekick UI:

```bash
kubectl port-forward -n observability svc/falco-falcosidekick-ui 2802:2802
```

The username password combination for the Falco Sidekick UI is *admin/admin*.

*Kiali* gives an overview of meshed services:

```bash
kubectl port-forward -n istio-system svc/kiali 20001:20001
```

The respective application UIs can be accessed using a browser by navigating to "http://localhost:{PORT}", where {PORT} is the forwarded port of the respective application.

## Troubleshooting

### Grafana restart loop

The Grafana pod sometimes hangs in a restart loop. You need to kill the pod to resolve this.

```bash
kubectl delete pod -n observability -l app.kubernetes.io/name=grafana
```

### Free Disk Space

If you run out of disk space on the VM you can clean all your unused docker images and volumes.

```bash
docker system prune --all --volumes
```

You can also disable and enable the MicroK8s registry to remove all stored images.

```bash
microk8s disable registry
microk8s enable registry
```

### Reinstall MicroK8s

If you have messed up the cluster completely you can simply reinstall it. Go through all the setup steps afterwards.

```bash
sudo snap remove microk8s --purge
sudo snap install microk8s --classic --channel=1.27/stable
```
