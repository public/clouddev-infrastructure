#!/usr/bin/env bash

kubectl -n observability rollout status deployment falco-falcosidekick
kubectl -n observability rollout status deployment falco-falcosidekick-ui
kubectl -n observability rollout status statefulset falco-falcosidekick-ui-redis
