#!/usr/bin/env bash

helm upgrade --install --namespace observability falco falcosecurity/falco -f values.yaml -f values-remote.yaml
. post-deploy.sh
