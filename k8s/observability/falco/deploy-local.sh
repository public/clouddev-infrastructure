#!/usr/bin/env bash

helm upgrade --install --namespace observability falco falcosecurity/falco -f values.yaml
. post-deploy.sh
