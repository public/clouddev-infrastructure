#!/usr/bin/env bash

helm upgrade --install --namespace observability loki-stack grafana/loki-stack -f values.yaml
kubectl -n observability rollout status statefulset loki-stack
