#!/usr/bin/env bash

helm upgrade --install --namespace observability promstack prometheus-community/kube-prometheus-stack -f values.yaml -f values-remote.yaml
kubectl apply -f certificate.yaml
. post-deploy.sh
. post-deploy-remote.sh
