#!/usr/bin/env bash

kubectl apply -n observability -f ceph-cluster-dashboard-configmap.yaml
kubectl apply -n observability -f ceph-osd-dashboard-configmap.yaml
kubectl apply -n observability -f ceph-pool-dashboard-configmap.yaml
