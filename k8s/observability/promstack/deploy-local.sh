#!/usr/bin/env bash

helm upgrade --install --namespace observability promstack prometheus-community/kube-prometheus-stack -f values.yaml
. post-deploy.sh
