#!/usr/bin/env bash

kubectl -n observability rollout status deployment promstack-grafana
kubectl -n observability rollout status deployment promstack-kube-state-metrics
kubectl -n observability rollout status deployment promstack-operator
kubectl -n observability rollout status statefulset prometheus-promstack-prometheus
kubectl -n observability rollout status statefulset alertmanager-promstack-alertmanager
kubectl apply -n observability -f flask-dashboard-configmap.yaml
kubectl apply -n observability -f istio-control-plane-dashboard-configmap.yaml
kubectl apply -n observability -f istio-mesh-dashboard-configmap.yaml
kubectl apply -n observability -f istio-service-dashboard-configmap.yaml
kubectl apply -n observability -f istio-wasm-extension-dashboard-configmap.yaml
kubectl apply -n observability -f istio-workload-dashboard-configmap.yaml
kubectl apply -n observability -f service-monitor-falco.yaml
