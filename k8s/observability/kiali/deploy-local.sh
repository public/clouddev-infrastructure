#!/usr/bin/env bash

helm upgrade --install \
    --namespace istio-system \
    kiali-server \
    kiali/kiali-server \
    -f values.yaml

kubectl -n istio-system rollout status deployment kiali
