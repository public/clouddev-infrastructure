#!/usr/bin/env bash

helm upgrade --install \
    --namespace istio-system \
    kiali-server \
    kiali/kiali-server \
    -f values.yaml \
    -f values-remote.yaml

kubectl -n istio-system rollout status deployment kiali
