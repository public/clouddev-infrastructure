#!/usr/bin/env bash

kubectl apply -n observability -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.42.0/jaeger-operator.yaml
kubectl -n observability rollout status deployment jaeger-operator
kubectl apply -n observability -f simplest.yaml
