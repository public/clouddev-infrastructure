nameOverride: gitlab
fullnameOverride: gitlab
componentName: gitlab

main:
  deployment:
    enabled: true

  updateStrategy:
    type: Recreate

  image:
    repository: gitlab/gitlab-ce
    tag: 16.7.0-ce.0

  containerName: gitlab

  terminationGracePeriodSeconds: 10

  containerPorts:
    - containerPort: 80
      name: http
    - containerPort: 22
      name: ssh
    - containerPort: 5005
      name: registry

  envVars:
    - name: GITLAB_OMNIBUS_CONFIG
      value: |
        external_url 'https://gitlab.eseidinger-cloud.de'
        nginx['listen_port'] = 80
        nginx['listen_https'] = false
        gitlab_rails['gitlab_shell_ssh_port'] = 30022
        registry_external_url 'https://registry.eseidinger-cloud.de'
        registry_nginx['listen_port'] = 5005 
        registry_nginx['listen_https'] = false

  extraVolumes:
    - name: gitlab-config
      persistentVolumeClaim:
        claimName: gitlab-config

  extraVolumeMounts:
    - name: gitlab-config
      mountPath: /etc/gitlab

service:
  enabled: true
  ports:
    - port: 80
      name: http

ingress:
  enabled: true
  ingressClassName: istio
  hostname: gitlab.eseidinger-cloud.de
  pathType: Prefix
  port: 80
  tls: true
  extraTls:
    - hosts:
        - gitlab.eseidinger-cloud.de
      secretName: gitlab-tls

persistence:
  enabled: true
  existingClaim: gitlab-data
  volumeMountName: gitlab-data
  mountPath: /var/opt/gitlab

extraDeploy:
  - |
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: gitlab-data
    spec:
      storageClassName: rook-cephfs
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: 5Gi
  - |
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: gitlab-config
    spec:
      storageClassName: rook-cephfs
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: 512Mi
  - |
    apiVersion: v1
    kind: Service
    metadata:
      name: gitlab-ssh
    spec:
      type: NodePort
      selector:
        app.kubernetes.io/name: gitlab
      ports:
        - protocol: TCP
          port: 22
          targetPort: 22
          nodePort: 30022
  - |
    apiVersion: v1
    kind: Service
    metadata:
      name: registry
    spec:
      type: ClusterIP
      selector:
        app.kubernetes.io/name: gitlab
      ports:
        - protocol: TCP
          port: 5005
          targetPort: 5005
  - |
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: registry
    spec:
      ingressClassName: istio
      rules:
      - host: registry.eseidinger-cloud.de
        http:
          paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: registry
                port:
                  number: 5005
      tls:
      - hosts:
          - registry.eseidinger-cloud.de
        secretName: registry-tls
