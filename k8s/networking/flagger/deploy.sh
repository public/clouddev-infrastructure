#!/usr/bin/env bash

kubectl apply -f https://raw.githubusercontent.com/fluxcd/flagger/main/artifacts/flagger/crd.yaml

helm upgrade -i flagger flagger/flagger \
    --namespace=istio-system \
    --set crd.create=false \
    --set meshProvider=istio \
    --set metricsServer=http://promstack-prometheus.observability.svc.cluster.local:9090

helm upgrade -i flagger-loadtester flagger/loadtester \
    --namespace=istio-system \
    --set istio.enabled=true
