
sudo dd if=/dev/zero of=/mnt/8GiB.swap bs=1024 count=8388608
sudo chmod 600 /mnt/8GiB.swap
sudo mkswap /mnt/8GiB.swap
sudo swapon /mnt/8GiB.swap
echo '/mnt/8GiB.swap swap swap defaults 0 0' | sudo tee -a /etc/fstab
