- name: Create VPN server
  hosts: localhost
  gather_facts: false

  tasks:
    - name: Create a firewall with rules
      hetzner.hcloud.firewall:
        name: vpn-firewall
        rules:
          - description: allow icmp from everywhere
            direction: in
            protocol: icmp
            source_ips:
              - 0.0.0.0/0
              - ::/0
          - description: allow ssh from everywhere
            direction: in
            protocol: tcp
            port: 22
            source_ips:
              - 0.0.0.0/0
              - ::/0
          - description: allow ssh (forward) from everywhere
            direction: in
            protocol: tcp
            port: 2222
            source_ips:
              - 0.0.0.0/0
              - ::/0
          - description: allow https from everywhere
            direction: in
            protocol: tcp
            port: 443
            source_ips:
              - 0.0.0.0/0
              - ::/0
          - description: allow Jupyter from everywhere
            direction: in
            protocol: tcp
            port: 8888
            source_ips:
              - 0.0.0.0/0
              - ::/0
          - description: allow OpenVPN from everywhere
            direction: in
            protocol: udp
            port: 1194
            source_ips:
              - 0.0.0.0/0
              - ::/0
        state: present
    
    - name: Create a basic ssh_key
      hetzner.hcloud.ssh_key:
        name: eseidinger_cloud admin key
        public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCkGpiAoEQTi5d70ZxMfnQvKW0ivW+oHLEbcA4BrxIbJHcEmbHRDPeIFPpzWXwVl9rpPihsKbOTsvxIyCDu3qzwGxLLX89KSc/pqRFyrWORfTfH+WNe2XiHT/orfFNO1pYpGBI6XyTypKrR2QH/8EIuNrdsL/zJxw9++H0c5y1SpqMF010/IXkaWu+ghbWki347Qd2LrJX+/kDxB0pA92oNKXry0DZtk9kqtcWK6l3x+V+8LGVL52NlUjeOqYvtSXNTG8T9iGpqgycEjQ5J53yfp6gFmjwyXfH9v6jxTN7xATRYECam3rpd3iL1SMA4s8xLJYKaXpacLHMeZR54pdgQ1TNpBAzcyfMCXPrq+InZ2n6UbKLffWYCodV84VeZsSc9VAOWHyO4U9RRplNKmXv6RHbnQ7DeoXo/EKOOa0TUKrWgBPDyJFigyJ8LCJBcKLJS/gM+kq7XATFfEPbNfXRl8oBD0/BtvTpU4YkHpy1bzyQ71ooRTfP+l+Ek9uxRHyFZlXz05V+BQzds/MYn8AEj346sE8uxoMKZsgRSr4VRKjEAhy+LNWczJWLuPUH3kaHY5B7EzNMFDEXfySxkEW3TIB0B9vtviVbdb6c6fo9Elcq0zT4SxPqBEqk2/XCZyzQbRNUibI4bh0N8DzlskDGxcD+41M2NW5liGUY7sI9r+w==
        state: present

    - name: Create VPN server
      register: vpn_server_result
      hetzner.hcloud.server:
        name: vpn
        state: present
        image: ubuntu-22.04
        server_type: cx11
        ssh_keys:
          - eseidinger_cloud admin key
        firewalls:
          - vpn-firewall

    - name: Delete DNS record for VPN server IPv4
      ignore_errors: true
      amazon.aws.route53:
        state: absent
        zone: eseidinger-cloud.de
        record: '*.eseidinger-cloud.de'
        type: A
        wait: true

    - name: Add DNS record for VPN server IPv4
      amazon.aws.route53:
        state: present
        zone: eseidinger-cloud.de
        record: '*.eseidinger-cloud.de'
        type: A
        ttl: 7200
        value:
          - "{{ vpn_server_result.hcloud_server.ipv4_address }}"
        wait: true

    - name: Delete DNS record for VPN server IP64
      ignore_errors: true
      amazon.aws.route53:
        state: absent
        zone: eseidinger-cloud.de
        record: '*.eseidinger-cloud.de'
        type: AAAA
        wait: true

    - name: Add DNS record for VPN server IPv6
      amazon.aws.route53:
        state: present
        zone: eseidinger-cloud.de
        record: '*.eseidinger-cloud.de'
        type: AAAA
        ttl: 7200
        value:
          - "{{ vpn_server_result.hcloud_server.ipv6 | regex_search('.+?(?=/64)') }}1"
        wait: true
