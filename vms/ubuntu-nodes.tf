terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu-qcow2-1" {
  name   = "ubuntu-qcow2-1"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
  format = "qcow2"
}

resource "libvirt_volume" "ubuntu-qcow2-2" {
  name   = "ubuntu-qcow2-2"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
  format = "qcow2"
}

resource "libvirt_volume" "ubuntu-qcow2-3" {
  name   = "ubuntu-qcow2-3"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
  format = "qcow2"
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud-init.yaml")
}

data "template_file" "network1_config" {
  template = file("${path.module}/network1.yaml")
}

data "template_file" "network2_config" {
  template = file("${path.module}/network2.yaml")
}

data "template_file" "network3_config" {
  template = file("${path.module}/network3.yaml")
}

# for more info about paramater check this out
# https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/website/docs/r/cloudinit.html.markdown
# Use CloudInit to add our ssh-key to the instance
# you can add also meta_data field
resource "libvirt_cloudinit_disk" "commoninit1" {
  name           = "commoninit1.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network1_config.rendered
  pool           = "default"
}

resource "libvirt_cloudinit_disk" "commoninit2" {
  name           = "commoninit2.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network2_config.rendered
  pool           = "default"
}

resource "libvirt_cloudinit_disk" "commoninit3" {
  name           = "commoninit3.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network3_config.rendered
  pool           = "default"
}

resource "libvirt_network" "nat_network" {
  name = "nat-net"
  mode = "nat"
  domain = "nat.local"
  addresses = [
    "192.168.42.0/24"
  ]
  dns {
    enabled = true
  }
}

# Create the machine
resource "libvirt_domain" "domain-ubuntu1" {
  name   = "ubuntu-k8s-1"
  memory = "8192"
  vcpu   = 4

  cloudinit = libvirt_cloudinit_disk.commoninit1.id

  network_interface {
    network_name = libvirt_network.nat_network.name
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.ubuntu-qcow2-1.id
  }
}

resource "libvirt_domain" "domain-ubuntu2" {
  name   = "ubuntu-k8s-2"
  memory = "8192"
  vcpu   = 4

  cloudinit = libvirt_cloudinit_disk.commoninit2.id

  network_interface {
    network_name = libvirt_network.nat_network.name
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.ubuntu-qcow2-2.id
  }
}

resource "libvirt_domain" "domain-ubuntu3" {
  name   = "ubuntu-k8s-3"
  memory = "8192"
  vcpu   = 4

  cloudinit = libvirt_cloudinit_disk.commoninit3.id

  network_interface {
    network_name = libvirt_network.nat_network.name
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.ubuntu-qcow2-3.id
  }
}

# IPs: use wait_for_lease true or after creation use terraform refresh and terraform show for the ips of domain
