# Creating a MicroK8s cluster using LXD

## Prerequisites

Enable IPv4 forwarding on the host machine

## Create the Cluster Manually

### Create loop devices

```bash
sudo mkdir -p /var/lib/lxd/disks
sudo truncate -s 3G /var/lib/lxd/disks/loop1.img
sudo truncate -s 3G /var/lib/lxd/disks/loop2.img
sudo truncate -s 3G /var/lib/lxd/disks/loop3.img

sudo losetup -f /var/lib/lxd/disks/loop1.img
sudo losetup -f /var/lib/lxd/disks/loop2.img
sudo losetup -f /var/lib/lxd/disks/loop3.img
```


### Create MicroK8s profile

```bash
lxc profile create microk8s
wget https://raw.githubusercontent.com/ubuntu/microk8s/master/tests/lxc/microk8s.profile -O microk8s.profile
cat microk8s.profile | lxc profile edit microk8s
rm microk8s.profile
```

### Create the LXD containers

```bash
lxc launch -p default -p microk8s ubuntu:22.04 k8s-1
lxc launch -p default -p microk8s ubuntu:22.04 k8s-2
lxc launch -p default -p microk8s ubuntu:22.04 k8s-3
```

### Attach the loop devices to the containers

```bash
lxc config device add k8s-1 loop1 unix-block source=/dev/loop10 path=/dev/sda
lxc config device add k8s-2 loop2 unix-block source=/dev/loop11 path=/dev/sda
lxc config device add k8s-3 loop3 unix-block source=/dev/loop12 path=/dev/sda
```

### Install MicroK8s inside the containers

```bash
lxc exec k8s-1 -- snap install microk8s --classic
lxc exec k8s-2 -- snap install microk8s --classic
lxc exec k8s-3 -- snap install microk8s --classic
```
