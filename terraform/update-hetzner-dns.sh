#!/usr/bin/env bash

nameservers=$(cat terraform.tfstate \
    | jq -r -c '.resources[].instances[].attributes.name_servers | select(. != null)' \
    | tr '[' ' ' | tr ']' ' ' | tr ',' ' ' | xargs)

primary_nameserver=$(cat terraform.tfstate \
    | jq -r -c '.resources[].instances[].attributes.primary_name_server | select(. != null)' \
    | xargs)


zone_id=$(curl "https://dns.hetzner.com/api/v1/zones" \
    -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}" \
    | jq -r -c '.zones[] | select(.name == "eseidinger-cloud.de") | .id')


records=$(curl "https://dns.hetzner.com/api/v1/records?zone_id=${zone_id}" \
    -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}" \
    | jq -r -c '.records[] | select(.type != "SOA") | .id')

for rec in ${records[@]}; do
    curl -X "DELETE" "https://dns.hetzner.com/api/v1/records/${rec}" \
        -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}"
done

soa_record=$(curl "https://dns.hetzner.com/api/v1/records?zone_id=${zone_id}" \
    -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}" \
    | jq -r -c '.records[] | select(.type == "SOA") | .id')

curl -X "PUT" "https://dns.hetzner.com/api/v1/records/${soa_record}" \
    -H "Content-Type: application/json" \
    -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}" \
    -d "{\"name\": \"@\", \"value\": \"${primary_nameserver}. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400\", \"type\": \"SOA\", \"zone_id\": \"${zone_id}\"}"

for ns in ${nameservers[@]}; do
    curl -X "POST" "https://dns.hetzner.com/api/v1/records" \
        -H "Content-Type: application/json" \
        -H "Auth-API-Token: ${HETZNER_DNS_TOKEN}" \
        -d "{\"name\": \"@\", \"value\": \"${ns}\", \"type\": \"NS\", \"zone_id\": \"${zone_id}\"}"
done
