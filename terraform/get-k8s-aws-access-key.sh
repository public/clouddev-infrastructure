#!/usr/bin/env bash

key_json=$(aws iam create-access-key --user-name eseidinger-cloud_de-k8s)
access_key_id=$(echo ${key_json} | jq -r .AccessKey.AccessKeyId)
secret_access_key=$(echo ${key_json} | jq -r .AccessKey.SecretAccessKey)

mkdir -p ../k8s/secret
echo export AWS_ACCESS_KEY_ID=${access_key_id} > ../k8s/secret/.env
echo export AWS_SECRET_ACCESS_KEY=${secret_access_key} >> ../k8s/secret/.env
