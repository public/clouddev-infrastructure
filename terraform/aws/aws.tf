terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}

variable "node1_ipv4_address" {

}

variable "node1_ipv6_address" {

}

variable "node2_ipv4_address" {

}

variable "node2_ipv6_address" {

}

variable "node3_ipv4_address" {

}

variable "node3_ipv6_address" {

}

variable "vpn_server_ipv4_address" {

}

variable "vpn_server_ipv6_address" {
  
}

variable "chatbot_server_ipv4_address" {

}

variable "chatbot_server_ipv6_address" {
  
}

resource "aws_route53_zone" "k8s" {
  name = "eseidinger-cloud.de"
}

resource "aws_route53_record" "k8s_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.node1_ipv4_address]
}

resource "aws_route53_record" "k8s_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.node1_ipv6_address]
}

resource "aws_route53_record" "k8s_all_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "*.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.node1_ipv4_address]
}

resource "aws_route53_record" "k8s_all_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "*.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.node1_ipv6_address]
}

resource "aws_route53_record" "k8s_node1_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-01.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.node1_ipv4_address]
}

resource "aws_route53_record" "k8s_node1_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-01.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.node1_ipv6_address]
}

resource "aws_route53_record" "k8s_node2_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-02.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.node2_ipv4_address]
}

resource "aws_route53_record" "k8s_node2_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-02.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.node2_ipv6_address]
}

resource "aws_route53_record" "k8s_node3_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-03.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.node3_ipv4_address]
}

resource "aws_route53_record" "k8s_node3_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "node-03.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.node3_ipv6_address]
}

resource "aws_route53_record" "k8s_vpn_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "vpn.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.vpn_server_ipv4_address]
}

resource "aws_route53_record" "k8s_vpn_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "vpn.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.vpn_server_ipv6_address]
}

resource "aws_route53_record" "k8s_chatbot_A" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "chatbot.eseidinger-cloud.de"
  type    = "A"
  ttl     = "7200"
  records = [var.chatbot_server_ipv4_address]
}

resource "aws_route53_record" "k8s_chatbot_AAAA" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "chatbot.eseidinger-cloud.de"
  type    = "AAAA"
  ttl     = "7200"
  records = [var.chatbot_server_ipv6_address]
}

resource "aws_route53_record" "k8s_MX" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "eseidinger-cloud.de"
  type    = "MX"
  ttl     = "7200"
  records = ["10 www358.your-server.de."]
}

resource "aws_route53_record" "k8s_TXT" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "eseidinger-cloud.de"
  type    = "TXT"
  ttl     = "7200"
  records = ["v=spf1 +a +mx ?all"]
}

resource "aws_route53_record" "k8s_autodiscover" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "_autodiscover._tcp.eseidinger-cloud.de"
  type    = "SRV"
  ttl     = "7200"
  records = ["0 100 443 mail.your-server.de."]
}

resource "aws_route53_record" "k8s_imaps" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "_imaps._tcp.eseidinger-cloud.de"
  type    = "SRV"
  ttl     = "7200"
  records = ["0 100 993 mail.your-server.de."]
}

resource "aws_route53_record" "k8s_pop3s" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "_pop3s._tcp.eseidinger-cloud.de"
  type    = "SRV"
  ttl     = "7200"
  records = ["0 100 995 mail.your-server.de."]
}

resource "aws_route53_record" "k8s_submission" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "_submission._tcp.eseidinger-cloud.de"
  type    = "SRV"
  ttl     = "7200"
  records = ["0 100 587 mail.your-server.de."]
}

resource "aws_route53_record" "k8s_autoconfig" {
  zone_id = aws_route53_zone.k8s.zone_id
  name    = "autoconfig.eseidinger-cloud.de"
  type    = "CNAME"
  ttl     = "7200"
  records = ["mail.your-server.de."]
}

resource "aws_iam_group_policy" "k8s_policy" {
  name  = "eseidinger-cloud_de-k8s-policy"
  group = aws_iam_group.k8s.name

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : ["route53:ListResourceRecordSets", "route53:ChangeResourceRecordSets"],
        "Resource" : aws_route53_zone.k8s.arn
      },
      {
        "Effect" : "Allow",
        "Action" : "route53:GetChange",
        "Resource" : "arn:aws:route53:::change/*"
      },
      {
        "Effect" : "Allow",
        "Action" : "route53:ListHostedZonesByName",
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetObject",
          "s3:PutObject",
          "s3:ListBucket"
        ],
        "Resource" : [
          aws_s3_bucket.backup_bucket.arn,
          "${aws_s3_bucket.backup_bucket.arn}/*"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "kms:GenerateDataKey",
          "kms:Decrypt"
        ],
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_s3_bucket" "backup_bucket" {
  bucket = "eseidinger-cloud.de-backup"
}

resource "aws_s3_bucket_ownership_controls" "backup_bucket_acl_ownership" {
  bucket = aws_s3_bucket.backup_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_acl" "backup_bucket_acl" {
  bucket = aws_s3_bucket.backup_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "backup_bucket_versioning" {
  bucket = aws_s3_bucket.backup_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "backup_bucket_lifecycle" {
  bucket = aws_s3_bucket.backup_bucket.id

  rule {
    id = "backup-default"

    expiration {
      days = 1
    }

    status = "Enabled"
  }
}

resource "aws_iam_group_membership" "k8s" {
  name = "eseidinger-cloud_de-k8s-group-membership"

  users = [
    aws_iam_user.k8s.name,
  ]

  group = aws_iam_group.k8s.name
}

resource "aws_iam_group" "k8s" {
  name = "eseidinger-cloud_de-k8s"
  path = "/eseidinger-cloud_de/"
}

resource "aws_iam_user" "k8s" {
  name = "eseidinger-cloud_de-k8s"
}
