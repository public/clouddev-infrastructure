terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.44.1"
    }
  }
}

output "node1_ipv4_address" {
  value = hcloud_server.eseidinger_cloud_node1.ipv4_address
}

output "node1_ipv6_address" {
  value = hcloud_server.eseidinger_cloud_node1.ipv6_address
}

output "node2_ipv4_address" {
  value = hcloud_server.eseidinger_cloud_node2.ipv4_address
}

output "node2_ipv6_address" {
  value = hcloud_server.eseidinger_cloud_node2.ipv6_address
}

output "node3_ipv4_address" {
  value = hcloud_server.eseidinger_cloud_node3.ipv4_address
}

output "node3_ipv6_address" {
  value = hcloud_server.eseidinger_cloud_node3.ipv6_address
}

output "vpn_server_ipv4_address" {
  value = hcloud_server.eseidinger_vpn_server.ipv4_address
}

output "vpn_server_ipv6_address" {
  value = hcloud_server.eseidinger_vpn_server.ipv6_address
}

output "chatbot_server_ipv4_address" {
  value = hcloud_server.eseidinger_chatbot_server.ipv4_address
}

output "chatbot_server_ipv6_address" {
  value = hcloud_server.eseidinger_chatbot_server.ipv6_address
}

variable "hcloud_token" {
  sensitive = true
}

variable "network_ip_range" {
  type    = string
  default = "10.5.0.0/16"
}

variable "subnet_ip_range" {
  type    = string
  default = "10.5.1.0/24"
}

provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_ssh_key" "eseidinger_cloud_ssh" {
  name       = "eseidinger_cloud admin key"
  public_key = file("~/.ssh/eseidinger_cloud_rsa.pub")
}

resource "hcloud_network" "eseidinger_cloud_net" {
  name     = "eseidinger_cloud internal network"
  ip_range = var.network_ip_range
}

resource "hcloud_network_subnet" "eseidinger_cloud_subnet" {
  type         = "cloud"
  network_id   = hcloud_network.eseidinger_cloud_net.id
  network_zone = "eu-central"
  ip_range     = var.subnet_ip_range
}


resource "hcloud_firewall" "eseidinger_cloud_firewall" {
  name = "eseidinger_cloud firewall"
  rule {
    direction = "in"
    protocol  = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  # SSH port will be used for git
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "30022"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "443"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  # Kubernetes API server
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "16443"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "any"
    source_ips = [
      "${var.subnet_ip_range}"
    ]
  }

  rule {
    direction = "in"
    protocol  = "udp"
    port      = "any"
    source_ips = [
      "${var.subnet_ip_range}"
    ]
  }
}

resource "hcloud_firewall" "eseidinger_vpn_firewall" {
  name = "eseidinger_vpn firewall"
  rule {
    direction = "in"
    protocol  = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "443"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  # VPN port
  rule {
    direction = "in"
    protocol  = "udp"
    port      = "1194"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }
}

resource "hcloud_firewall" "eseidinger_chatbot_firewall" {
  name = "eseidinger_chatbot firewall"
  rule {
    direction = "in"
    protocol  = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "any"
    source_ips = [
      "${var.subnet_ip_range}"
    ]
  }

  rule {
    direction = "in"
    protocol  = "udp"
    port      = "any"
    source_ips = [
      "${var.subnet_ip_range}"
    ]
  }
}

resource "hcloud_server" "eseidinger_cloud_node1" {
  name         = "node-01"
  image        = "ubuntu-22.04"
  server_type  = "cx41"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.eseidinger_cloud_ssh.id]
  firewall_ids = [hcloud_firewall.eseidinger_cloud_firewall.id]

  network {
    network_id = hcloud_network.eseidinger_cloud_net.id
    ip         = "10.5.1.1"
  }

  depends_on = [
    hcloud_network_subnet.eseidinger_cloud_subnet
  ]
}

resource "hcloud_server" "eseidinger_cloud_node2" {
  name         = "node-02"
  image        = "ubuntu-22.04"
  server_type  = "cx41"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.eseidinger_cloud_ssh.id]
  firewall_ids = [hcloud_firewall.eseidinger_cloud_firewall.id]

  network {
    network_id = hcloud_network.eseidinger_cloud_net.id
    ip         = "10.5.1.2"
  }

  depends_on = [
    hcloud_network_subnet.eseidinger_cloud_subnet
  ]
}

resource "hcloud_server" "eseidinger_cloud_node3" {
  name         = "node-03"
  image        = "ubuntu-22.04"
  server_type  = "cx41"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.eseidinger_cloud_ssh.id]
  firewall_ids = [hcloud_firewall.eseidinger_cloud_firewall.id]

  network {
    network_id = hcloud_network.eseidinger_cloud_net.id
    ip         = "10.5.1.3"
  }

  depends_on = [
    hcloud_network_subnet.eseidinger_cloud_subnet
  ]
}

resource "hcloud_server" "eseidinger_vpn_server" {
  name         = "vpn"
  image        = "ubuntu-22.04"
  server_type  = "cx11"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.eseidinger_cloud_ssh.id]
  firewall_ids = [hcloud_firewall.eseidinger_vpn_firewall.id]
}

resource "hcloud_server" "eseidinger_chatbot_server" {
  name         = "chatbot"
  image        = "ubuntu-22.04"
  server_type  = "cpx31"
  backups      = false
  ssh_keys     = [hcloud_ssh_key.eseidinger_cloud_ssh.id]
  firewall_ids = [hcloud_firewall.eseidinger_chatbot_firewall.id]

  network {
    network_id = hcloud_network.eseidinger_cloud_net.id
    ip         = "10.5.1.201"
  }

  depends_on = [
    hcloud_network_subnet.eseidinger_cloud_subnet
  ]
}

resource "hcloud_volume" "eseidinger_cloud_volume1" {
  name      = "volume-01"
  size      = 50
  server_id = hcloud_server.eseidinger_cloud_node1.id
  automount = false
  format    = "ext4"
}

resource "hcloud_volume" "eseidinger_cloud_volume2" {
  name      = "volume-02"
  size      = 50
  server_id = hcloud_server.eseidinger_cloud_node2.id
  automount = false
  format    = "ext4"
}

resource "hcloud_volume" "eseidinger_cloud_volume3" {
  name      = "volume-03"
  size      = 50
  server_id = hcloud_server.eseidinger_cloud_node3.id
  automount = false
  format    = "ext4"
}
