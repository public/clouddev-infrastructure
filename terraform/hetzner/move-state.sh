#!/usr/bin/env bash

states=$(terraform state list)

for state in ${states[@]}; do
    terraform state mv -state-out ../terraform.tfstate ${state} module.hetzner.${state}
done
