# Terraform Configuration

Update the `secret/.env` following the template in `secret/.env.template`.

```bash
source secret/.env
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
bash update-hetzner-dns.sh
bash get-k8s-aws-access-key.sh
```
