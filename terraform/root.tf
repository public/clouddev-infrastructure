variable "hcloud_token" {
  sensitive = true
}

module "hetzner" {
  source       = "./hetzner"
  hcloud_token = var.hcloud_token
}

module "aws" {
  source                      = "./aws"
  node1_ipv4_address          = module.hetzner.node1_ipv4_address
  node1_ipv6_address          = module.hetzner.node1_ipv6_address
  node2_ipv4_address          = module.hetzner.node2_ipv4_address
  node2_ipv6_address          = module.hetzner.node2_ipv6_address
  node3_ipv4_address          = module.hetzner.node3_ipv4_address
  node3_ipv6_address          = module.hetzner.node3_ipv6_address
  vpn_server_ipv4_address     = module.hetzner.vpn_server_ipv4_address
  vpn_server_ipv6_address     = module.hetzner.vpn_server_ipv6_address
  chatbot_server_ipv4_address = module.hetzner.chatbot_server_ipv4_address
  chatbot_server_ipv6_address = module.hetzner.chatbot_server_ipv6_address
}
